#!/bin/sh

#Common aliases

alias md='mkdir'

alias agu='sudo apt-get update'
alias agg='sudo apt-get upgrade'
alias agd='sudo apt-get dist-upgrade'
alias install='sudo apt-get install'
alias remove='sudo apt-get remove'
alias search='apt-cache search'
alias hist='history | grep'
alias hdump='hexdump -v -e '\''"%010_ad  |" 16/1 "%_p" "|\n"'\'''

