#!/bin/sh

function mount_cholatse {
	for d in Backup Contacts Documents Downloads Films Music Numerisations Photos Pictures Public Templates Videos Workspaces
	do
		[[ -d /home/sherpa/.mnt/cholatse/$d ]] || mkdir -p /home/sherpa/.mnt/cholatse/$d
		[[ -n $( mount | grep "$d on ") ]]		|| sshfs sherpa@cholatse:/mnt/Namche\ Bazar/$d		/home/sherpa/.mnt/cholatse/$d
	done
}

function umount_cholatse {
	for d in Backup Contacts Documents Downloads Films Music Numerisations Photos Pictures Public Templates Videos Workspaces
	do
		[[ -n $( mount | grep "$d on ") ]]		&& fusermount -u /home/sherpa/.mnt/cholatse/$d
	done
}
