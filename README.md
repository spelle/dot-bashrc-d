# dot-bashrc-d

## Installation

Clone the repo :
* via SSH

```
sudo git clone git@framagit.org:spelle/dot-bashrc-d.git /etc/bashrc.spelle.d
```

* via HTTPS

```
sudo git clone https://framagit.org/spelle/dot-bashrc-d.git /etc/bashrc.spelle.d
```

Run following command :

```
cat /etc/bashrc.spelle.d/K00-Install.sh | sh
```

