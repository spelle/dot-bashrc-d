#!/bin/sh

cat << EOF | tee -a ~/.bashrc

if [ -d ~/.bashrc.spelle.d ]; then
  for i in ~/.bashrc.spelle.d/S*.sh; do
    if [ -r \$i ]; then
      . \$i
    fi
  done
  unset i
fi
EOF

cat << EOF | tee -a ~/.zshrc

if [ -d ~/.bashrc.spelle.d ]; then
  for i in ~/.bashrc.spelle.d/S*.sh; do
    if [ -r \$i ]; then
      . \$i
    fi
  done
  unset i
fi
EOF
